﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml.Media.Imaging;
using System.IO;
using Windows.Graphics.Imaging;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml;
using Windows.UI.Core;
using Windows.Storage.Streams;

namespace HlWorklist.Examination.ViewModels
{
    public class ExaminationVM : ObservableObject
    {
        CoreDispatcher myDispatcher;
        ImageSource myImageSource;
        public ExaminationVM()
        {
            myDispatcher = Window.Current.Dispatcher;
            //ExaminationImage = new WriteableBitmap(1024, 1024);
            Locator.Connection.ExaminationImageChanged += Connection_ExaminationImageChanged;
        }

        private async void Connection_ExaminationImageChanged(object sender, Services.ExaminationImageEventArgs e)
        {
            await myDispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                using (MemoryStream memStream = new MemoryStream(e.PixelData))
                {
                    var image = new BitmapImage();
                    await image.SetSourceAsync(memStream.AsRandomAccessStream());
                    ExaminationImage = image;
                }

                //using (Stream stream = ((WriteableBitmap)ExaminationImage).PixelBuffer.AsStream())
                //{
                //    await stream.WriteAsync(e.PixelData, 0, e.PixelData.Length);
                //}
            });
        }

        public ImageSource ExaminationImage
        {
            get { return myImageSource; }
            set {
                myImageSource = value;
                OnPropertyChanged();
            }
        }
    }
}
