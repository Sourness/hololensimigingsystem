﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace HlWorklist.Common.Behaviours
{
    public class SelectionChanged
    {
        public static ICommand GetCommand(ListView obj)
        {
            return (ICommand)obj.GetValue(CommandProperty);
        }

        public static void SetCommand(ListView obj, ICommand value)
        {
            obj.SetValue(CommandProperty, value);
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.RegisterAttached("Command", typeof(ICommand), typeof(SelectionChanged), new PropertyMetadata(null, OnCommandPropertyChanged));

        private static void OnCommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var listView = (ListView)d;

            if (e.NewValue != null)
            {
                listView.SelectionChanged += ListView_SelectionChanged;
            }
            else
            {
                listView.SelectionChanged -= ListView_SelectionChanged;
            }
        }

        private static void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listView = (ListView)sender;

            var command = GetCommand(listView);

            var parameter = listView.SelectedValue;
            if (command != null && command.CanExecute(parameter)) {
                command.Execute(parameter);
            }
        }

    }
}
