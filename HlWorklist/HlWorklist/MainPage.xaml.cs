﻿using HlWorklist.Communication;
using HlWorklist.PatientBrowser.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.ServiceModel;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HlWorklist
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            //Locator.NavigationService = new Services.NavigationService(myFrame);

            this.Loaded += MainPage_Loaded;
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //Locator.NavigationService.NavigateTo(typeof(PatientBrowserView));
        }

        //public async void ValueChanged(string value)
        //{
        //    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
        //    {
        //        textBlock.Text = value;
        //    });
        //}

        //HoloLensClient _client;
        //private async void button1_Click(object sender, RoutedEventArgs e)
        //{
        //    var localFolder = ApplicationData.Current.LocalFolder;

        //    IStorageFile file = null;
        //    var item = await localFolder.TryGetItemAsync("ip.txt");

        //    if (item == null)
        //    {
        //        file = await localFolder.CreateFileAsync("ip.txt", CreationCollisionOption.ReplaceExisting);
        //        await FileIO.WriteTextAsync(file, "192.168.1.135:8733");
        //    }
        //    else
        //    {
        //        file = await localFolder.GetFileAsync("ip.txt");
        //    }
        //    var address = await FileIO.ReadTextAsync(file);

        //    _client = new HoloLensClient(new System.ServiceModel.InstanceContext(this), address);
        //    var factory = (DuplexChannelFactory<IHololensIo>)(_client.ChannelFactory);
        //    var channel = factory.CreateChannel();
        //    channel.Connect();
        //}

        private void button_Click(object sender, RoutedEventArgs e)
        {
           // _client.SetValue(DateTime.Now.ToString());
        }
    }
}
