﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HlWorklist.Data
{
    [DataContract(Namespace ="Data")]
    public class Patient : ObservableObject
    {
        public DateTime ExaminationDate { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string InsuranceProvider { get; set; }
        public IList<Procedure> Procedures { get; } = new List<Procedure>();

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }
        public string PlanTime { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        public string ClinicalProcedure { get; set; }

        [DataMember]
        public string PatientId { get; set; }

        [DataMember]
        public string UniqueId { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }
        public string BirthDateGender { get { return DateOfBirth + ", " + this.Gender; } }

        public string Template { get; set; }

        string myCurrentAcquisitionType;
        public string CurrentAcquisitionType
        {
            get { return myCurrentAcquisitionType; }
            set {
                myCurrentAcquisitionType = value;
                OnPropertyChanged();
            }
        }

        public override bool Equals(object obj)
        {
            Patient patient = obj as Patient;

            return patient != null && string.Equals(FirstName, patient.FirstName) && string.Equals(LastName, patient.LastName);
        }

        public override int GetHashCode()
        {
            string str = string.Format("{0} {1}", FirstName, LastName);
            return str.GetHashCode();
        }
    }
}
