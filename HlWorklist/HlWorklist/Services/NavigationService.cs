﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;

namespace HlWorklist.Services
{
    public class NavigationService
    {
        SystemNavigationManager navigationManager = SystemNavigationManager.GetForCurrentView();

        Frame _frame;
        public NavigationService(Frame frame)
        {
            _frame = frame;
            navigationManager.BackRequested += NavigationManager_BackRequested;
        }

        internal void GoBack()
        {
            if (_frame.CanGoBack)
            {
                _frame.GoBack();
            }
        }

        private void NavigationManager_BackRequested(object sender, BackRequestedEventArgs e)
        {
        }

        public void NavigateTo(Type pageType)
        {
            _frame.Navigate(pageType);
        }
    }
}
