﻿using HlWorklist.Examination.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace HlWorklist.Services
{
public class ExaminationService
    {
        CoreApplicationView myExaminationView;
        public void StartExamination()
        {
            CreateExaminationView();
            Locator.Connection.StartExamination();
        }

        private async void CreateExaminationView()
        {
            var currentView = ApplicationView.GetForCurrentView();
            myExaminationView = CoreApplication.CreateNewView();

            
            await myExaminationView.Dispatcher.RunAsync(
                CoreDispatcherPriority.Normal,
                async () =>
                {
                    var newWindow = Window.Current;
                    var newAppView = ApplicationView.GetForCurrentView();
                    newAppView.Title = "New window";

                    newWindow.Closed += NewWindow_Closed;
                    var frame = new Frame();
                    frame.Navigate(typeof(ExaminationPage), null);
                    newWindow.Content = frame;
                    newWindow.Activate();

                    await ApplicationViewSwitcher.TryShowAsStandaloneAsync(
                        newAppView.Id,
                        ViewSizePreference.UseMinimum,
                        currentView.Id,
                        ViewSizePreference.UseMinimum);
                });
        }

        private void NewWindow_Closed(object sender, CoreWindowEventArgs e)
        {
            
        }

    }
}
