﻿using HlWorklist.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HlWorklist.Data;
using Windows.Storage;
using System.ServiceModel;
using Windows.ApplicationModel.Core;

namespace HlWorklist.Services
{
    public class ExaminationImageEventArgs : EventArgs
    {
        public byte[] PixelData;
        public int Width;
        public int Height;
        public string PixelFormat;
    }

    public class UisPConnectionService : IHololensIoCallback, IHololensIo
    {
        public event EventHandler<ExaminationImageEventArgs> ExaminationImageChanged;
        public event EventHandler<WorklistDataToSend> WorklistChanged;

        private HoloLensClient myClient;

        public HoloLensClient Client
        {
            get {
                if(myClient == null)
                {
                    ConnectAsync();
                }
                return myClient;
            }
            //set { myClient = value; }
        }

        public async Task ConnectAsync()
        {
            await Task.Run(() =>
            {
                Connect();
            });
        }

        public async void Connect()
        {
            var endPointAddress = await GetEndpointAddress();

            //await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            //{
            try
            {
                myClient = new HoloLensClient(new System.ServiceModel.InstanceContext(this), endPointAddress);
                var factory = (DuplexChannelFactory<IHololensIo>)(Client.ChannelFactory);
                var channel = factory.CreateChannel();
                factory.Faulted += Factory_Faulted;
                channel.Connect();
                var worklistData = channel.GetWorklist();
                WorklistSchemeChanged(worklistData);

            }
            catch
            {
                myClient = null;
            };
        }

        private void Factory_Faulted(object sender, EventArgs e)
        {
            myClient = null;
        }

        private async Task<string> GetEndpointAddress()
        {
            var localFolder = ApplicationData.Current.LocalFolder;

            IStorageFile file = null;
            var item = await localFolder.TryGetItemAsync("ip.txt");

            if (item == null)
            {
                file = await localFolder.CreateFileAsync("ip.txt", CreationCollisionOption.ReplaceExisting);
                await FileIO.WriteTextAsync(file, "192.168.1.135:8733");
            }
            else
            {
                file = await localFolder.GetFileAsync("ip.txt");
            }

            //var address = "192.168.1.135:8733";
            var address = await FileIO.ReadTextAsync(file);

            return address;
        }

        public void WorklistSchemeChanged(WorklistDataToSend newWorklist)
        {
            if (newWorklist != null)
            {
                WorklistChanged?.Invoke(this, newWorklist);
            }
        }

        public void ExaminationResultImage(byte[] pixelData, int width, int height, string pixelFormat)
        {
            ExaminationImageChanged?.Invoke(this, new ExaminationImageEventArgs { PixelData = pixelData, Width = width, Height = height, PixelFormat = pixelFormat });
        }

        public void UpdateControlScheme(WorklistDataToSend dataReceived)
        {
            try
            {
                Client.UpdateControlScheme(dataReceived);
            }
            catch (CommunicationObjectFaultedException)
            {
                myClient = null;
            }
        }

        public void UpdateSelection(string uniqueIdOfPatient)
        {
            try
            {
                Client.UpdateSelection(uniqueIdOfPatient);
            }
            catch (CommunicationObjectFaultedException)
            {
                myClient = null;
            }
        }

        public void StartExamination()
        {
            try
            {
                Client.StartExamination();
            }
            catch (CommunicationObjectFaultedException)
            {
                myClient = null;
            }
        }

        public WorklistDataToSend GetWorklist()
        {
            try
            {
                return Client.GetWorklist();
            }
            catch (CommunicationObjectFaultedException)
            {
                myClient = null;
            }
            return null;
        }
    }
}
