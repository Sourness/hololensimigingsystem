﻿using HlWorklist.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HlWorklist
{
    public class Locator
    {
        public static NavigationService NavigationService { get; internal set; }

        static ExaminationService myExamination;
        public static ExaminationService Examination
        {
            get { return myExamination ?? (myExamination = new ExaminationService()); }
        }

        static UisPConnectionService myConnection;

        public static UisPConnectionService Connection
        {
            get { return myConnection ?? (myConnection = new UisPConnectionService()); }
        }

    }
}
