﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using HlWorklist.Data;

namespace HlWorklist.Communication
{
    public partial class HoloLensClient : DuplexClientBase<IHololensIo>, IHololensIo
    {
        static partial void ConfigureEndpoint(ServiceEndpoint serviceEndpoint, ClientCredentials clientCredentials);

        public HoloLensClient(InstanceContext callbackInstance, string address) :
                base(callbackInstance, new NetTcpBinding(SecurityMode.None) { ReceiveTimeout = TimeSpan.FromSeconds(10), SendTimeout = TimeSpan.FromSeconds(10), MaxReceivedMessageSize = int.MaxValue }, new EndpointAddress(string.Format("net.tcp://{0}/HololensIo", address.Trim())))
        {
            this.Endpoint.Name = "NetTcpBinding_IHololensIo";
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public void Connect()
        {
            base.Channel.Connect();
        }

        public void UpdateControlScheme(WorklistDataToSend dataReceived)
        {
            base.Channel.UpdateControlScheme(dataReceived);
        }

        public void UpdateSelection(string uniqueIdOfPatient)
        {
            base.Channel.UpdateSelection(uniqueIdOfPatient);
        }

        public void StartExamination()
        {
            base.Channel.StartExamination();
        }

        public WorklistDataToSend GetWorklist()
        {
            return base.Channel.GetWorklist();
        }
    }
}
