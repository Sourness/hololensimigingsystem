﻿using HlWorklist.Common;
using HlWorklist.Communication;
using HlWorklist.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace HlWorklist.PatientBrowser.ViewModels
{
    public class PatientBrowserVM : ObservableObject
    {
        public PatientBrowserVM()
        {
            Locator.Connection.WorklistChanged += Connection_WorklistChanged;
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "10:45", LastName = "Singleton", FirstName = "Faith", Gender = "Female", DateOfBirth = new DateTime(1943, 8, 29), ClinicalProcedure = "Standard Screening", PatientId = "1", ExaminationDate = new DateTime(2010, 8, 15), InsuranceProvider = "Sehr gute Krankenkase", SocialSecurityNumber = "10-542-54285"/*, Procedures = new List<Procedure>() { standardScreeningNoTomo }*/ });
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "10:55", LastName = "Hoffman", FirstName = "Marie", Gender = "Female", DateOfBirth = new DateTime(1959, 3, 9), ClinicalProcedure = "Standard Screening", PatientId = "2", ExaminationDate = new DateTime(2010, 09, 15), InsuranceProvider = "Sehr gute Krankenkase 2", SocialSecurityNumber = "10-542-54285"/*, Procedures = new List<Procedure>() { standardScreeningNoTomo }*/ });
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "11:05", LastName = "Sharp", FirstName = "Victoria", Gender = "Female", DateOfBirth = new DateTime(1951, 7, 26), ClinicalProcedure = "Standard Screening", PatientId = "3a", ExaminationDate = new DateTime(2010, 08, 17), InsuranceProvider = "Allianz", SocialSecurityNumber = "20-542-54285"/*, Procedures = new List<Procedure>() { standardScreeningNoTomo }*/ });
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "11:20", LastName = "Graham", FirstName = "Petra", Gender = "Female", DateOfBirth = new DateTime(1954, 5, 22), ClinicalProcedure = "Standard Screening", PatientId = "3b", ExaminationDate = new DateTime(2010, 8, 17), InsuranceProvider = "Allianz", SocialSecurityNumber = "20-542-54285"/*, Procedures = new List<Procedure>() { standardScreeningNoTomo } */});
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "11:45", LastName = "Goeppert-Mayer", FirstName = "Maria", Gender = "Female", DateOfBirth = new DateTime(1972, 2, 20), ClinicalProcedure = "Tomo Screening", PatientId = "4", ExaminationDate = new DateTime(2010, 08, 11), InsuranceProvider = "Allianz", SocialSecurityNumber = "10-254-54796"/*, Procedures = new List<Procedure>() { standardScreeningTomo } */});
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "13:30", LastName = "Amano", FirstName = "Victoria", Gender = "Female", DateOfBirth = new DateTime(1960, 9, 11), ClinicalProcedure = "Tomo Screening", PatientId = "5", ExaminationDate = new DateTime(2010, 08, 17), InsuranceProvider = "Hala bala", SocialSecurityNumber = "10-542-54285"/*, Procedures = new List<Procedure>() { standardScreeningTomo } */});
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "13:45", LastName = "Smith", FirstName = "Martha", Gender = "Female", DateOfBirth = new DateTime(1929, 1, 30), ClinicalProcedure = "Tomo Screening", PatientId = "6", ExaminationDate = new DateTime(2010, 8, 11), InsuranceProvider = "Sehr gute Krankenkase", SocialSecurityNumber = "10-542-54285"/*, Procedures = new List<Procedure>() { standardScreeningTomo } */});
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "10:55", LastName = "Cori", FirstName = "Gerty Theresa", Gender = "Female", DateOfBirth = new DateTime(1985, 3, 9), ClinicalProcedure = "FD Bilateral Mammogram", PatientId = "7", ExaminationDate = new DateTime(2015, 09, 12), InsuranceProvider = "Sehr gute Krankenkase 2", SocialSecurityNumber = "10-542-54285"/*, Procedures = new List<Procedure>() { standardScreeningNoTomo } */});
            //Patients.Add(new Patient { CurrentAcquisitionType = "Mammogram", Template = "WorkflowTemplates\\DefaultMammoScreening.xml", PlanTime = "11:20", LastName = "Barre-Sinoussi", FirstName = "Francoise", Gender = "Female", DateOfBirth = new DateTime(1978, 5, 22), ClinicalProcedure = "FD Bilateral Mammogram", PatientId = "8", ExaminationDate = new DateTime(2015, 8, 17), InsuranceProvider = "Allianz", SocialSecurityNumber = "20-542-54285"/*, Procedures = new List<Procedure>() { standardScreeningNoTomo } */});
        }

        private async void Connection_WorklistChanged(object sender, WorklistDataToSend e)
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                // check, whether patients exists in collection
                //
                if (e.Worklist != null)
                {
                    List<Patient> patientsToRemove = new List<Patient>();
                    foreach(var patient in Patients)
                    {
                        if (!e.Worklist.Contains(patient))
                        {
                            patientsToRemove.Add(patient);
                        }
                    }

                    foreach (var patient in patientsToRemove)
                    {
                        Patients.Remove(patient);
                    }

                    foreach (var patient in e.Worklist)
                    {
                        if (!Patients.Contains(patient))
                        {
                            Patients.Add(patient);
                        }
                    }
                }
            });
        }

        public ObservableCollection<Patient> Patients { get; } = new ObservableCollection<Patient>();

        public ObservableCollection<string> AcquisitionTypeList { get; } = new ObservableCollection<string> { "Wall", "Table", "Ortho Mode", "Tomography", "Free Detector", "Free Cassette", "Mammogram" };

        ICommand _myExamCommand;
        public ICommand ExamCommand
        {
            get
            {
                return _myExamCommand ?? (_myExamCommand = new DelegateCommand(p => {
                    Locator.Examination.StartExamination();
                }));
            }
        }

        ICommand _myActivePatientChanged;
        public ICommand ActivePatientChanged
        {
            get
            {
                return _myActivePatientChanged ?? (_myActivePatientChanged = new DelegateCommand(p => {
                    Task.Run(() =>
                    {
                        Patient patient = p as Patient;
                        Locator.Connection.UpdateSelection(patient != null ? patient.UniqueId : string.Empty);
                    });
                }));
            }
        }
    }
}
