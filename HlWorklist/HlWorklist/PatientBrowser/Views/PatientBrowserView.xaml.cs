﻿using HlWorklist.Communication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.ServiceModel;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HlWorklist.Data;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HlWorklist.PatientBrowser.Views
{
    public sealed partial class PatientBrowserView : UserControl
    {

        public PatientBrowserView()
        {
            this.InitializeComponent();
            this.Loaded += PatientBrowserView_Loaded;
        }

        private async void PatientBrowserView_Loaded(object sender, RoutedEventArgs e)
        {
            await Locator.Connection.ConnectAsync();
        }
    }
}
