﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HlWorklist.Data
{
    public class Procedure : ObservableObject
    {
        public string Id { get; set; }
        public string BodyArea { get; set; }
        public string Indication { get; set; }
        public string ClinicalProcedure { get; set; }
        public IList<ClinicalProcedureStep> Images { get; } = new List<ClinicalProcedureStep>();
        public bool IsFluoro { get; set; }
    }
}
