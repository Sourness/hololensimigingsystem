﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HlWorklist.Data
{
    [DataContract(Namespace = "Data")]
    public class WorklistDataToSend
    {
        [DataMember]
        public IList<Patient> Worklist;

        [DataMember] 
        public int SelectedIndex { get; set; }
    }
}
