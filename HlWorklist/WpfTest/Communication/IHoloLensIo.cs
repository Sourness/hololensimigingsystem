﻿using HlWorklist.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WpfTest.Communication
{
    [ServiceContract(ConfigurationName = "HlWorklist.Communication.IHololensIo", CallbackContract = typeof(IHololensIoCallback))]
    public interface IHololensIo
    {
        [OperationContract]
        void UpdateControlScheme(WorklistDataToSend dataReceived);

        [OperationContract]
        void UpdateSelection(string uniqueIdOfPatient);

        [OperationContract]
        void StartExamination();

        [OperationContract]
        void Connect();
    }

    public interface IHololensIoCallback
    {
        [OperationContract]
        void WorklistSchemeChanged(WorklistDataToSend newWorklist);

        [OperationContract]
        void ExaminationResultImage(byte[] pixelData, int width, int height, string pixelFormat);
    }
}
