﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HlWorklist.Data;
using WpfTest.Communication;

namespace WpfTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public partial class MainWindow : Window, IHololensIo
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        IHololensIoCallback _callbackChannel;
        public void Connect()
        {
            _callbackChannel = OperationContext.Current.GetCallbackChannel<IHololensIoCallback>();
        }

        public string GetValue()
        {
            return DateTime.Now.ToShortDateString();
        }

        public void SetValue(string value)
        {
            textBox.Text = value;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            NetTcpBinding b = new NetTcpBinding();
            b.Security.Mode = SecurityMode.None;
            b.Security.Message.ClientCredentialType = MessageCredentialType.Certificate;

            Uri httpsAddress = new Uri("net.tcp://localhost:8733/HoloLensIo");
            ServiceHost sh = new ServiceHost(this, httpsAddress);
            sh.AddServiceEndpoint(typeof(IHololensIo), b, httpsAddress);
            sh.Open();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(_callbackChannel != null)
            {
                _callbackChannel.WorklistSchemeChanged(null);

            }
        }

        public void UpdateControlScheme(WorklistDataToSend dataReceived)
        {
            throw new NotImplementedException();
        }

        public void UpdateSelection(string uniqueIdOfPatient)
        {
            throw new NotImplementedException();
        }

        public void StartExamination()
        {
            throw new NotImplementedException();
        }
    }
}
